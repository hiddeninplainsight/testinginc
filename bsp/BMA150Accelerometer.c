#include "BMA150Accelerometer.h"
#include "i2c.h"

struct Raw3DSensorData BMA150Accelerometer_ReadAcceleration(void)
{
	const unsigned char BMA150Address = 0x38;
	if(!I2C_Start(BMA150Address))
	{
		I2C_Stop();
		return;
	}

	const unsigned char registerAddress = 0x02;
	I2C_Write(registerAddress);

	if(!I2C_Start(BMA150Address | 0x01))
	{
		I2C_Stop();
		return;
	}

	struct Raw3DSensorData rawAcceleration;
	unsigned char* raw = (unsigned char*)&rawAcceleration;

	I2C_Read(raw, true);
	raw++;
	I2C_Read(raw, true);
	raw++;
	I2C_Read(raw, true);
	raw++;
	I2C_Read(raw, true);
	raw++;
	I2C_Read(raw, true);
	raw++;
	I2C_Read(raw, false);

	I2C_Stop();

	rawAcceleration.x >>= 6;
	rawAcceleration.y >>= 6;
	rawAcceleration.z >>= 6;

	return rawAcceleration;
}
