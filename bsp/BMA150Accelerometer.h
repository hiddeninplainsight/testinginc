#ifndef BMA150_ACCELEROMETER_H
#define BMA150_ACCELEROMETER_H

#include "Raw3DSensorData.h"

struct Raw3DSensorData BMA150Accelerometer_ReadAcceleration(void);

#endif // #ifndef BMA150_ACCELEROMETER_H
