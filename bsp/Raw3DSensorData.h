#ifndef RAW_3D_SENSOR_DATA_H
#define RAW_3D_SENSOR_DATA_H

struct Raw3DSensorData
{
	short x;
	short y;
	short z;
};

#endif // #ifndef RAW_3D_SENSOR_DATA_H
