#ifndef FFF_EXTRA_H
#define FFF_EXTRA_H

#include "fff.h"

#define RUN_TEST(test) setup(); \
                       printf("%s\n", #test); \
                       fflush(stdout); \
                       test(); \
                       teardown()


#define PP_NARG(...)     PP_NARG_(__VA_ARGS__, PP_RSEQ_N())
#define PP_NARG_(...)     PP_ARG_N(__VA_ARGS__)
#define PP_ARG_N(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, N, ...)   N
#define PP_RSEQ_N()     20,19,18,17,16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0

#define VERIFY_CALLS1(index, func) assert(fff.call_history[index] == (void*)func);
#define VERIFY_CALLS2(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS1(index + 1, __VA_ARGS__)
#define VERIFY_CALLS3(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS2(index + 1, __VA_ARGS__)
#define VERIFY_CALLS4(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS3(index + 1, __VA_ARGS__)
#define VERIFY_CALLS5(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS4(index + 1, __VA_ARGS__)
#define VERIFY_CALLS6(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS5(index + 1, __VA_ARGS__)
#define VERIFY_CALLS7(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS6(index + 1, __VA_ARGS__)
#define VERIFY_CALLS8(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS7(index + 1, __VA_ARGS__)
#define VERIFY_CALLS9(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS8(index + 1, __VA_ARGS__)
#define VERIFY_CALLS10(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS9(index + 1, __VA_ARGS__)
#define VERIFY_CALLS11(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS10(index + 1, __VA_ARGS__)
#define VERIFY_CALLS12(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS11(index + 1, __VA_ARGS__)
#define VERIFY_CALLS13(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS12(index + 1, __VA_ARGS__)
#define VERIFY_CALLS14(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS13(index + 1, __VA_ARGS__)
#define VERIFY_CALLS15(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS14(index + 1, __VA_ARGS__)
#define VERIFY_CALLS16(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS15(index + 1, __VA_ARGS__)
#define VERIFY_CALLS17(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS16(index + 1, __VA_ARGS__)
#define VERIFY_CALLS18(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS17(index + 1, __VA_ARGS__)
#define VERIFY_CALLS19(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS18(index + 1, __VA_ARGS__)
#define VERIFY_CALLS20(index, func, ...) assert(fff.call_history[index] == (void*)func); VERIFY_CALLS19(index + 1, __VA_ARGS__)


#define VERIFY_CALLS(...)     VERIFY_CALLS_(PP_NARG(__VA_ARGS__), __VA_ARGS__); \
                              assert(fff.call_history_idx == PP_NARG(__VA_ARGS__))

#define VERIFY_CALLS_(N,...)     VERIFY_CALLS_N(N,__VA_ARGS__)
#define VERIFY_CALLS_N(N,...)     VERIFY_CALLS ## N(0, __VA_ARGS__)



#define HISTORY_NAME(func, arg_index) func ## _fake.arg ## arg_index ## _history
#define HISTORY_LENGTH(func) func ## _fake.call_count

#define VERIFY_HISTORY1(history, index, value, ...) assert(history[index] == value)
#define VERIFY_HISTORY2(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY1(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY3(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY2(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY4(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY3(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY5(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY4(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY6(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY5(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY7(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY6(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY8(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY7(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY9(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY8(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY10(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY9(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY11(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY10(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY12(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY11(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY13(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY12(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY14(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY13(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY15(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY14(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY16(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY15(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY17(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY16(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY18(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY17(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY19(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY18(history, index + 1, __VA_ARGS__)
#define VERIFY_HISTORY20(history, index, value, ...) assert(history[index] == value); VERIFY_HISTORY19(history, index + 1, __VA_ARGS__)

#define VERIFY_ARG_HISTORY(func, arg, ...)   VERIFY_HISTORY_(HISTORY_NAME(func, arg), PP_NARG(__VA_ARGS__), __VA_ARGS__); \
                                             assert(HISTORY_LENGTH(func) == PP_NARG(__VA_ARGS__))

#define VERIFY_HISTORY_(history, N,...)      VERIFY_HISTORY_N(history, N,__VA_ARGS__)
#define VERIFY_HISTORY_N(history, N,...)     VERIFY_HISTORY ## N(history, 0, __VA_ARGS__)


#endif
