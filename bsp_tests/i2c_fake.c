#include "i2c_fake.h"


DEFINE_FAKE_VALUE_FUNC(bool, I2C_Start, uint8_t);
DEFINE_FAKE_VALUE_FUNC(bool, I2C_Write, uint8_t);
DEFINE_FAKE_VOID_FUNC(I2C_Read, uint8_t*, bool);
DEFINE_FAKE_VOID_FUNC(I2C_Stop);


static uint8_t const* readData;
static void Custom_Read(uint8_t* data, bool ack)
{
  *data = readData[I2C_Read_fake.call_count - 1];
}

void I2CFake_SetReadData(uint8_t const* data)
{
  I2C_Read_fake.custom_fake = Custom_Read;
  readData = data;
}

void I2CFake_Reset(void)
{
  RESET_FAKE(I2C_Start);
  RESET_FAKE(I2C_Write);
  RESET_FAKE(I2C_Read);
  RESET_FAKE(I2C_Stop);

  I2C_Start_fake.return_val = true;
  I2C_Write_fake.return_val = true;

  readData = NULL;
}

