#ifndef I2C_FAKE_H
#define I2C_FAKE_H

#include "i2c.h"
#include "fff_extra.h"

DECLARE_FAKE_VALUE_FUNC(bool, I2C_Start, uint8_t);
DECLARE_FAKE_VALUE_FUNC(bool, I2C_Write, uint8_t);
DECLARE_FAKE_VOID_FUNC(I2C_Read, uint8_t*, bool);
DECLARE_FAKE_VOID_FUNC(I2C_Stop);

void I2CFake_SetReadData(uint8_t const* data);
void I2CFake_Reset(void);

#endif
