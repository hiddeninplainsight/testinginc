#include "BMA150Accelerometer.h"

#include "i2c_fake.h"

#include "fff_extra.h"
#include "fff.h"

#include <stdio.h>
#include <assert.h>


static void setup()
{
  I2CFake_Reset();

  FFF_RESET_HISTORY();
}

static void teardown()
{
}

static void I2CFake_Verify_Working_ReadAcceleration(void)
{
  VERIFY_CALLS(I2C_Start, I2C_Write,
               I2C_Start, I2C_Read, I2C_Read, I2C_Read, I2C_Read, I2C_Read, I2C_Read,
               I2C_Stop);

  // If you don't want to use the macro above then you can check each call individually
  assert(fff.call_history[0] == (void*)I2C_Start);

  assert(I2C_Start_fake.arg0_history[0] == 0x38);
  assert(I2C_Start_fake.arg0_history[1] == 0x39);

  assert(I2C_Write_fake.arg0_history[0] == 0x02);

  VERIFY_ARG_HISTORY(I2C_Read, 1, true, true, true, true, true, false );
}


static void testBMA150Accelerometer_Reading_an_acceleration_of_0(void)
{
  // Given
  const unsigned char readData[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
  I2CFake_SetReadData(readData);

  // When
  struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

  // Then
  I2CFake_Verify_Working_ReadAcceleration();
  assert(result.x == 0);
  assert(result.y == 0);
  assert(result.z == 0);
}

static void testBMA150Accelerometer_Reading_an_acceleration_of_1_in_all_directions(void)
{
  // Given
  const unsigned char readData[] = { 0x40, 0x00, 0x40, 0x00, 0x40, 0x00 };
  I2CFake_SetReadData(readData);

  // When
  struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

  // Then
  I2CFake_Verify_Working_ReadAcceleration();
  assert(result.x == 1);
  assert(result.y == 1);
  assert(result.z == 1);
}

static void testBMA150Accelerometer_Reading_different_accelerations_in_all_directions(void)
{
  // Given
  const unsigned char readData[] = { 0xC0, 0xFF, 0x40, 0x00, 0x00, 0x00 };
  I2CFake_SetReadData(readData);

  // When
  struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

  // Then
  I2CFake_Verify_Working_ReadAcceleration();
  assert(result.x == -1);
  assert(result.y == 1);
  assert(result.z == 0);
}

static void testBMA150Accelerometer_Device_not_responding_to_write_address_is_stopped_after_sending_address(void)
{
  // Given
  I2C_Start_fake.return_val = false;

  // When
  struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

  // Then
  VERIFY_CALLS(I2C_Start, I2C_Stop);
}

static void testBMA150Accelerometer_Device_not_responding_to_read_address_is_stopped_after_sending_address(void)
{
  // Given
  bool start_acks[] = { true, false };
  SET_RETURN_SEQ(I2C_Start, start_acks, 2);

  // When
  struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

  // Then
  VERIFY_CALLS(I2C_Start, I2C_Write, I2C_Start, I2C_Stop);
}


int main()
{
  RUN_TEST(testBMA150Accelerometer_Reading_an_acceleration_of_0);
  RUN_TEST(testBMA150Accelerometer_Reading_an_acceleration_of_1_in_all_directions);
  RUN_TEST(testBMA150Accelerometer_Reading_different_accelerations_in_all_directions);
  RUN_TEST(testBMA150Accelerometer_Device_not_responding_to_write_address_is_stopped_after_sending_address);
  RUN_TEST(testBMA150Accelerometer_Device_not_responding_to_read_address_is_stopped_after_sending_address);

  printf("All tests passed!\n");
  return 0;
}
