#ifndef I2C_H
#define I2C_H

#include <stdbool.h>
#include <stdint.h>

bool I2C_Start(uint8_t address);
bool I2C_Write(uint8_t data);
void I2C_Read(uint8_t* data, bool ack);
void I2C_Stop(void);

#endif
